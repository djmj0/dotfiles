#!/bin/bash
# baraction.sh for spectrwm status bar


## Kernel
kernel() {
  kernel="$(uname -r)"
  echo -e "Linux - $kernel"
}

## DISK
hdd() {
  hdd="$(df -h | awk 'NR==8{print $3, $5}')"
  echo -e "SSD: $hdd"
}

## RAM
mem() {
  mem=`free | awk '/Mem/ {printf "%dM/%dM\n", $3 / 1024.0, $2 / 1024.0 }'`
  echo -e "$mem"
}

## CPU
cpu() {
  read cpu a b c previdle rest < /proc/stat
  prevtotal=$((a+b+c+previdle))
  sleep 0.5
  read cpu a b c idle rest < /proc/stat
  total=$((a+b+c+idle))
  cpu=$((100*( (total-prevtotal) - (idle-previdle) ) / (total-prevtotal) ))
  echo -e "CPU: $cpu%"
}

## VOLUME
vol() {
    vol=`amixer get Master | awk -F'[][]' 'END{ print $4":"$2 }' | sed 's/on://g'`
    echo -e "VOL: $vol"
}

## NETWORK TRAFFIC
update() {
	sum=0
	for arg; do
		read -r i < "$arg"
		sum=$(( sum + i ))
	done
	cache=${XDG_CACHE_HOME:-$HOME/.cache}/${1##*/}
	[ -f "$cache" ] && read -r old < "$cache" || old=0
	printf %d\\n "$sum" > "$cache"
	printf %d\\n $(( sum - old ))
}
ntt() {
	rx=$(update /sys/class/net/[ew]*/statistics/rx_bytes)
	tx=$(update /sys/class/net/[ew]*/statistics/tx_bytes)
	printf "(DN)%4sB (UP)%4sB\\n" $(numfmt --to=iec $rx) $(numfmt --to=iec $tx)
}

SLEEP_SEC=3
#loops forever outputting a line every SLEEP_SEC secs

# It seems that we are limited to how many characters can be displayed via
# the baraction script output. And the the markup tags count in that limit.
# So I would love to add more functions to this script but it makes the 
# echo output too long to display correctly.
#while :; do
#    echo "+@fg=1; +@fn=1;💻+@fn=0; $(cpu) +@fg=0; | +@fg=2; +@fn=1;💾+@fn=0; $(mem) +@fg=0; | +@fg=3; +@fn=1;💿+@fn=0; $(hdd) +@fg=0; |"
#	sleep $SLEEP_SEC
#done

while :; do
	echo "+@fg=5; +@fn=0; $(ntt) +@fg=0; | +@fg=1; +@fn=0; $(cpu) +@fg=0; | +@fg=4; +@fn=0; $(kernel) +@fg=0; |  +@fg=2; +@fn=0; $(mem) +@fg=0; | +@fg=3; +@fn=0; $(hdd) +@fg=0; |"
    sleep $SLEEP_SEC
done
