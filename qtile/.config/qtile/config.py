# -*- coding: utf-8 -*-
#  _ _     _  _            _  ___
# | | | __| |(_)_ __ ___  (_)/ _ \
# | | |/ _` || | '_ ` _ \ | | | | |
# | | | (_| || | | | | | || | |_| |
# | | |\__,_|/ |_| |_| |_|/ |\___/
# |_|_|    |__/         |__/
#
# A customized config.py for Qtile window manager (http://www.qtile.org)
# Modified by Danny (http://www.gitlab.com/djmj0/)
#
# The following comments are the copyright and licensing information from the default
# qtile config. Copyright (c) 2010 Aldo Cortesi, 2010, 2014 dequis, 2012 Randall Ma,
# 2012-2014 Tycho Andersen, 2012 Craig Barnes, 2013 horsik, 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this
# software and associated documentation files (the "Software"), to deal in the Software
# without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be included in all copies
# or substantial portions of the Software.

##### IMPORTS #####
import os
import re
import socket
import subprocess
from libqtile.config import Key, Screen, Group, Drag, Click
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from typing import List  # noqa: F401

##### DEFINING SOME VARIABLES #####
mod = "mod4"                                            # Sets mod key to SUPER/WINDOWS
myTerm = "alacritty"                                    # My terminal of choice
myConfig = "/home/zeus/.config/qtile/config.py"         # The Qtile config file location
myAutostart ="/home/zeus/.config/qtile/autostart.sh"    # My autostart file location
##### KEYBINDINGS #####
keys = [
         ### The essentials
         Key(
             [mod], "Return",
             lazy.spawn(myTerm),
             desc='Launches my terminal'
             ),
         Key(
             [mod], "d",
             lazy.spawn("rofi -show drun"),
             desc='Rofi DRun Launcher'
             ),
         Key(
             [mod], "r",
             lazy.spawn("rofi -show run"),
             desc='Rofi Run Launcher'
             ),
         Key(
             [mod], "i",
             lazy.next_layout(),
             desc='Toggle through layouts'
             ),
         Key(
             [mod], "u",
             lazy.prev_layout(),
             desc='Toggle through layouts'
             ),
         Key(
             [mod], "x",
             lazy.window.kill(),
             desc='Kill active window'
             ),
         Key(
             [mod, "shift"], "r",
             lazy.restart(),
             desc='Restart Qtile'
             ),
         Key(
             [mod, "shift"], "q",
             lazy.shutdown(),
             desc='Shutdown Qtile'
             ),
         Key(
             [mod], "l",
             lazy.spawn("betterlockscreen -l dimblur -t 'The olympic gates are closed'"),
             desc='Lock the screen with betterlockscreen'
             ),
         ### Treetab controls
         Key([mod, "control"], "k",
             lazy.layout.section_up(),
             desc='Move up a section in treetab'
             ),
         Key([mod, "control"], "j",
             lazy.layout.section_down(),
             desc='Move down a section in treetab'
             ),
         ### Window controls
         Key(
             [mod], "j",
             lazy.layout.down(),
             desc='Move focus down in current stack pane'
             ),
         Key(
             [mod], "k",
             lazy.layout.up(),
             desc='Move focus up in current stack pane'
             ),
         Key(
             [mod], "a",
             lazy.screen.prev_group(),
             desc='Move to the prev group'
             ),
         Key(
             [mod], "e",
             lazy.screen.next_group(),
             desc='Move to the next group'
             ),
         Key(
             [mod, "shift"], "j",
             lazy.layout.shuffle_down(),
             desc='Move windows down in current stack'
             ),
         Key(
             [mod, "shift"], "k",
             lazy.layout.shuffle_up(),
             desc='Move windows up in current stack'
             ),
         Key(
             [mod], "g",
             lazy.layout.grow(),
             desc='Expand window (MonadTall), increase number in master pane (Tile)'
             ),
         Key(
             [mod], "s",
             lazy.layout.shrink(),
             desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
             ),
         Key(
             [mod], "n",
             lazy.layout.normalize(),
             desc='normalize window size ratios'
             ),
         Key(
             [mod], "m",
             lazy.layout.maximize(),
             desc='toggle window between minimum and maximum sizes'
             ),
         Key(
             [mod, "shift"], "f",
             lazy.window.toggle_floating(),
             desc='toggle floating'
             ),
         ### Stack controls
         Key(
             [mod, "shift"], "space",
             lazy.layout.flip(),
             desc='Switch which side main pane occupies (MonadTall)'
             ),
         Key(
             [mod], "space",
             lazy.layout.next(),
             desc='Switch window focus to other pane(s) of stack'
             ),
         Key(
             [mod, "control"], "Return",
             lazy.layout.toggle_split(),
             desc='Toggle between split and unsplit sides of stack'
             ),
         ### My applications launched with SUPER + ALT + KEY
         Key(
             [mod], "F1",
             lazy.spawn(myTerm+" -e ranger"),
             desc='ranger file browser'
             ),
         Key(
             [mod], "F2",
             lazy.spawn("thunar"),
             desc='Thunar'
             ),
         Key(
             [mod], "F3",
             lazy.spawn("brave"),
             desc='brave browser'
             ),
         Key(
             [mod], "F4",
             lazy.spawn("evolution"),
             desc='evolution'
             ),
         Key(
             [mod], "F5",
             lazy.spawn("keepassxc"),
             desc='KeepassXC'
             ),
         Key(
             [mod], "F6",
             lazy.spawn(myTerm+ "-e /home/zeus/bin/AppImages/standard-notes-3.4.6-linux-x86_64.AppImage"),
             desc='Standard Notes'
             ),
         Key(
             [mod], "F7",
             lazy.spawn("rawtherapee"),
             desc='Rawtherapee'
             ),
         Key(
             [mod], "F8",
             lazy.spawn(myTerm+" -e cmus"),
             desc='cmus'
             ),
         Key(
             [mod], "F9",
             lazy.spawn(myTerm+" -e newsboat"),
             desc='start newsboat rss feeder'
             ),
         Key(
             [mod], "F10",
             lazy.spawn(myTerm+" -e nvim /home/zeus/dotfiles/nvim/.config/nvim/init.vim"),
             desc='start my init.vim'
             ),
         Key(
             [mod], "F11",
             lazy.spawn(myTerm+" -e nvim /home/zeus/dotfiles/qtile/.config/qtile/config.py"),
             desc='start my qtile config.py'
             ),
         Key(
             [mod, "mod1"], "b",
             lazy.spawn(myTerm+" -e bpytop"),
             desc='start bpytop'
             )
]

##### GROUPS #####
group_names = [("WWW", {'layout': 'max'}),
               ("DEV", {'layout': 'monadtall'}),
               ("SYS", {'layout': 'monadwide'}),
               ("MSG", {'layout': 'max'}), #
               ("STN", {'layout': 'max'}),
               ("DOC", {'layout': 'monadtall'}),
               ("VID", {'layout': 'monadtall'}),
               ("DIV", {'layout': 'monadtall'}),
               ("MSC", {'layout': 'monadtall'})]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group

##### DEFAULT THEME SETTINGS FOR LAYOUTS ##### play with parameters
layout_theme = {"border_width": 4,
                "margin": 24,
                "border_focus": "40C4FF",
                "border_normal": "1D2330"
                }

## Backup Colors pink
#layout_theme = {"border_width": 4,
#                "margin": 24,
#                "border_focus": "e1acff",
#                "border_normal": "1D2330"
#                }


##### THE LAYOUTS #####
layouts = [
    #layout.Bsp(**layout_theme),
    #layout.Columns(**layout_theme),
    #layout.RatioTile(**layout_theme),
    #layout.VerticalTile(**layout_theme),
    #layout.Matrix(**layout_theme),
    #layout.Tile(shift_windows=True, **layout_theme),
    #layout.Floating(**layout_theme),
    layout.Max(**layout_theme),
    layout.Stack(**layout_theme),
    layout.MonadWide(**layout_theme),
    layout.MonadTall(**layout_theme),
    #layout.Zoomy(**layout_theme, columnwidth=95)
    #layout.TreeTab(
    #     font = "mononoki Nerd Font Regular",
    #     fontsize = 16,
    #     sections = ["FIRST","SECOND"],
    #     section_fontsize = 24,
    #     bg_color = "141414",
    #     active_bg = "e1acff",
    #     active_fg = "000000",
    #     inactive_bg = "384323",
    #     inactive_fg = "a0a0a0",
    #     padding_y = 5,
    #     padding_x = 7,
    #     margin_left = 7,
    #     section_top = 10,
    #     panel_width = 250
    #     )
]

##### COLORS #####  DELETE UNUSED
colors = [["", ""], # panel background
          ["434758", "434758"], # background for current screen tab
          ["40C4FF", "40C4FF"], # font color for group names
          ["69F0AE", "69F0AE"], # border line color for current window name
          ["", ""], # border line color for other tab and odd widgets
          ["", ""], # color for the even widgets
          ["69F0AE", "69F0AE"]] # window name


## Backup Colors
#colors = [["#280a36", "#282a36"], # panel background
#          ["#434758", "#434758"], # background for current screen tab
#          ["#9999ff", "#9999ff"], # font color for group names
#          ["#ff5555", "#ff5555"], # border line color for current tab
#          ["#8d62a9", "#8d62a9"], # border line color for other tab and odd widgets
#          ["#668bd7", "#668bd7"], # color for the even widgets
#          ["#e1acff", "#e1acff"]] # window name


##### PROMPT ##### No idea what this does yet
# prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

##### DEFAULT WIDGET SETTINGS #####
widget_defaults = dict(
    font="Noto Sans Medium",#Mononoki Nerd Font Regular
    fontsize = 15,
    padding = 2,
    background = "393946" #666699
)
extension_defaults = widget_defaults.copy()

### MOUSE CALLBACKS ###
def open_yaupg(qtile):
    qtile.cmd_spawn('alacritty -e pakku -Syu')

def open_python(qtile):
    qtile.cmd_spawn('chromium https://docs.python.org/3/')

### WIDGETS ###
def init_widgets_list():
    widget_list = [
                widget.Sep(
                        linewidth = 0,
                        padding = 2
                        ),
                widget.Image(
                        filename = "~/.config/qtile/icons/Python-icon.png",
                        mouse_callbacks = {'Button1': open_python},
                        padding = 10
                        ),
                widget.GroupBox(
                        font = "Noto Sans Medium", #Mononoki
                        margin_y = 3,
                        fontsize = 15,
                        margin_x = 0,
                        padding_y = 5,
                        padding_x = 5,
                        borderwidth = 3,
                        active = colors[2],
                        inactive = "e0e0eb",
                        rounded = False,
                        highlight_color = colors[3],
                        highlight_method = "line",
                        foreground = colors[2]
                        ),
               # widget.Prompt(
               #         ),
                widget.CurrentLayoutIcon(
                        custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")],
                        padding = 0,
                        scale=0.7
                        ),
                widget.CurrentLayout(
                        padding = 5
                        ),
                widget.WindowName(
                        foreground = colors[2],
                        padding = 5,
                        show_state = True
                        ),
                widget.Net(
                        interface = "wlp1s0",
                        format = '{down} ↓↑ {up}',
                        padding = 5,
                        foreground = colors[6]
                        ),
               # widget.Wlan(
               #         foreground = colors[2],
               #         padding = 5,
               #         update = 1,
               #         interface = "wlp1s0",
               #         disconnected_message = ' ',
               #         format = '{percent:2.0%}'# 
               #         ),
                widget.CPUGraph(
                        graph_color = colors[2],
                        border_color = colors[1]
                        ),
                #widget.TextBox(
                #        text=" ⟳",
                #        padding = 0,
                #        foreground = colors[2],
                #        fontsize = 22,
                #        mouse_callbacks = {'Button1': open_yaupg}
                #        ),
                #widget.CheckUpdates(
                #        distro = 'Arch_pakku',
                #        mouse_callbacks = {'Button1': open_yaupg},
                #        colour_have_updates = "ff5555",
                #        colour_no_updates = colors[2],
                #        display_format = '{updates}',
                #        update_interval = 1800,
                #        padding = 5
                #        ),
                widget.Cmus(
                        play_color = colors[6],
                        padding = 5,
                        max_chars = 30
                        ),
                #widget.Pomodoro(
                #        color_inactive = colors[2],
                #        padding = 5
                #        ),
                widget.Clock(
                        format='%d/%m/%y %a %H:%M',
                        foreground = colors[2]
                        ),
                widget.Systray(
                        padding = 5
                        )
               # widget.Sep(
               #         linewidth = 0,
               #         padding = 7 
               #         )
                ]
    return widget_list

##### SCREENS #####

def init_screen():
    return [Screen(top=bar.Bar(widgets=init_widgets_list(), opacity=0.88, size=24))]

screens = init_screen()

##### DRAG FLOATING WINDOWS #####
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

##### FLOATING WINDOWS #####
floating_layout = layout.Floating(float_rules=[
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"

@hook.subscribe.startup_once
def startup():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([home])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "qtile" #default: LG3D
