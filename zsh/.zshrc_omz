# If you come from bash you might have to change your $PATH.
export PATH=".":/home/zeus/anaconda3/bin:/home/zeus/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/home/zeus/bin:/opt
# Path to your oh-my-zsh installation.
export ZSH="/home/zeus/.oh-my-zsh"

# Export dbus session
export $(dbus-launch)
# Export no at bridge if no a11y (accessibility tools) are necessary
export NO_AT_BRIDGE=1

# Insert powerline font usability to zsh, see Arch Linux Wiki
powerline-daemon -q
. /usr/lib/python3.8/site-packages/powerline/bindings/zsh/powerline.zsh


# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="" #gnzh, astro, banana, common, elessar, guri, phalanx, phi, pi, purify, purity, theto, spaceship

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(archlinux git history-substring-search colored-man-pages zsh-vim-mode zsh-autosuggestions zsh-syntax-highlighting) # geometry

source $ZSH/oh-my-zsh.sh

# Enable colors and change prompt:
autoload -U colors && colors	# Load colors
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "
setopt autocd		# Automatically cd into typed directory.
stty stop undef		# Disable ctrl-s to freeze terminal.


# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

#set emacs as default editor
# export ALTERNATE_EDITOR=""
# Preferred editor for local and remote sessions
 if [[ -n $SSH_CONNECTION ]]; then
   export EDITOR='nvim'
 else
   export EDITOR='nvim'
 fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

## ALIASES

# Pacman aliases
#alias install='sudo pacman -S'
#alias update='sudo pacman -Syu'
#alias uninstall='sudo pacman -Rsc'
#alias search='sudo pacman -Ss'
#alias uninstallorphans='sudo pacman -R $(pacman -Qdtq)'
alias cacheclear='sudo pacman -Sc'

# weather pulls with wttr.in
alias muc='curl wttr.in/Munich'
alias stl='curl wttr.in/Santiago,Chile'

# system command aliases
alias zshrc='nvim ~/dotfiles/zsh/.zshrc'
alias ete='etesync-dav'
alias v='nvim'
alias ..='cd ..'
alias qtile='nvim ~/dotfiles/qtile/.config/qtile/config.py'
alias cmat='cmatrix -C magenta'
alias dust='dust -rs'
alias yd='youtube-dl --config-location ~/.config/youtube-dl/config'
alias air='sudo hummingbird ~/Documents/AirVPN/AirVPN_Germany_UDP-443.ovpn'
alias ttyc='tty-clock -c -C 5'
alias st='speedtest --secure'
alias vc='nvim ~/dotfiles/nvim/.config/nvim/init.vim'
alias pfetch='export PF_INFO="ascii title os host kernel uptime pkgs wm" && pfetch'
alias pt='python'

# commands for use with s6 software suite
alias sdbs='sudo s6-rc-db list services'            # List all available services in database without bundles
alias sdba='sudo s6-rc-db list all'                 # List all available services and bundles in database
alias sdbb='sudo s6-rc-db contents default'         # List all services in bundle 'default'
alias ssl='sudo s6-rc -a list'                      # List all active services
alias ssu='sudo s6-rc -u change'                    # Activate service(s) for current session
alias ssd='sudo s6-rc -d change'                    # Deactivate service(s) for current session
alias sba='sudo s6-rc-bundle-update add default'    # Add service(s) to 'default' bundle for automated initialization at boot
alias sbd='sudo s6-rc-bundle-update delete default' # Remove service(s) from 'default' bundle and from automated initiliazation at boot

# git push dotfiles to repo
alias dg='cd ~/dotfiles/ && git add . && git commit -m "Update the whole repo on $(date +"%A %F")" && git push'

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing

# Borgbackup-command
alias bb='~/bin/borgbackup.sh'
alias bc='~/bin/borgcheck.sh'
alias bl='borg list /run/media/zeus/Borgbackup/Borgrepo'
alias bm='borg mount /run/media/zeus/Borgbackup/Borgrepo /home/zeus/borg' # Mount borgbackup full repo to borg folder
alias bu='borg umount /home/zeus/borg'

# Other backup commands
alias fpbup='~/bin/androidbackup.sh'    # Fairphone backup command
alias cfgbup='~/bin/config_backup.sh'   # .config backup to Mediaserver with tar

# Startup at every newly opened terminal
# neofetch

### RANDOM COLOR SCRIPT ###
colorscript random

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/zeus/anaconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/zeus/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/home/zeus/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/zeus/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
