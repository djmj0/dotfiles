# dotfiles

Here are all of my config files that I use to configure my personal laptop. I am using Artixlinux as my host OS and qtile as my WM. 

## License

    The files found in this repository are used to configure certain programs working on my PC.
    Copyright (C) 2020 djmj0

    The files are free software: you can redistribute them and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    These files are distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see https://www.gnu.org/licenses/ .
